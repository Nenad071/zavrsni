<?php



define("_PATH_",'includes/');
define("_PGS_",'pages/');
define("APP_DIR", __DIR__);

spl_autoload_register('myAutoloader');

function myAutoloader($className)
{
    include APP_DIR."/classes/".$className.'.php';
}

include "helpers/securityHelper.php";

Session::Start();
$conn=Database::Connect();

if (isset($_SESSION['user']))
{
    $user= User::UnserializeUser();
}

$skolaStrane = array
    (
        "0"=>"predmeti.php",
        "1"=>"get_by_kategorija.php",
        "2"=>"loginForm.php",
        "3"=>"formRegistration.php",
        "4"=>"acessDenied.php",
        "5"=> "shoppingCart.php",
        "6"=>"checkout.php",
        "7"=>"Error404.php",
        "8"=>"get_by_subject.php",
        "9"=>"profile.php", 
        
    
        "11"=>"formKategorije.Admin.php",
        "12"=>"formPredmeti.Admin.php",
        "13"=>"adminIndex.php",
        "14"=>"formKorisnici.Admin.php"
 
    );
