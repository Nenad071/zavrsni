<?php

class Database
{   
    public static $conn;
    public $table;
    public $fields;
    public $preparedValues;
    public $valuesForInsert=array();
    
     public static function Connect()
    {
         try{
             $conn = new PDO("mysql:host=localhost;dbname=vezbanje", "root","");
            self::$conn=$conn;  
         }
         
         catch (Exception $e){
             echo $e->getMessage();
         }
         
        
        
    }
    
    private function PrepareInsertValues()
    {

        $fieldsArr = explode(",", $this->fields);

        $insertValues=array();

        //$properOrderedArray = array_merge(array_flip( $fieldsArr), $this->valuesForInsert);
        $properOrderedArray = array_replace(array_flip( $fieldsArr), $this->valuesForInsert);
        

        foreach ($properOrderedArray as $k=>$v)
        {
            if (in_array($k, $fieldsArr))
            {
                 $insertValues[]= $v;    
            } 
        }
       return $insertValues;
    }
    
    public function Insert()
    {
        $stmt = self::$conn->prepare
        (
            "INSERT INTO {$this->table} ({$this->fields}) 
             VALUES ({$this->preparedValues})"
        );
           
        $insertValues = $this->PrepareInsertValues(); 
       
   
        if ($stmt->execute($insertValues))
        {   
            return self::$conn->lastInsertId();
        } 
        else 
        {    
            return false;
        }
    }
    
    public function Update()
    {
       
        $updateString="";
        $this->fields=explode(",", $this->fields);
        
        foreach ($this->fields as $v)
        {
            $updateString.= $v." = ?,";
        }
        $updateString=rtrim($updateString,",");
        
        $stmt = self::$conn->prepare
        (
            "UPDATE  {$this->table} SET {$updateString} WHERE {$this->primaryKey} = ? {$this->filter}"
        );
        
        $this->fields=implode(",",$this->fields);
        $insertValues = $this->PrepareInsertValues();

        $insertValues[]=$this->id;
        return $stmt->execute($insertValues);  
    }
    
    public function Delete()
    {
        $stmt = self::$conn->prepare
        (
            "DELETE from {$this->table} WHERE {$this->primaryKey} = ? {$this->filter}" 
        );
            
        $insertValues=array($this->id);
        return $stmt->execute($insertValues);
    }
    
    public function GetAll()
    {        
       $sth = self::$conn->prepare("SELECT {$this->fields} from {$this->table} where 1 {$this->filter}");

       $sth->execute();
       $result = $sth->fetchAll(PDO::FETCH_CLASS, get_called_class());
       
       return $result; 
       
    }

    public function GetById()
    {
       $sth = self::$conn->prepare("SELECT {$this->fields} from {$this->table} where {$this->primaryKey} = ? {$this->filter}" );
       $sth->execute(array($this->id));
       //$result = $sth->fetchAll(PDO::FETCH_OBJ);
        $result = $sth->fetchAll(PDO::FETCH_CLASS, get_called_class()); #get_called_class()
       
        if (count($result) ==0)
            return false;
        
        if (count($result)<2 && count($result)>0)
        return $result[0]; 
        else   
        return $result;
    } 
}
