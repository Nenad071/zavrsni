<?php

class View 
{
    private function GetHead()
    {
        include _PATH_."head.php";
    }
    
    private function GetWrapper()
    {
        include _PATH_."divWrapper.php";
        $this->GetHeader();
        $this->GetBrowse();
        $this->GetKategorije();
        //$this->GetSearch();
        $this->NavCondition();
        //$this->GetInner();
    }
    
    private function GetHeader()
    {
        global $user;
        include _PATH_."header.php";
    }
    
    private function GetBrowse()
    {
        include _PATH_."browse.php";
    }
      
    private function GetKategorije()
    {
        global $user;
        include _PATH_."kategorije.php";
    }
    
    private function GetSearch()
    {
        include _PATH_."search.php";
    }
    
    private function GetNavigation()
    {
        include _PATH_."navigation.php";
    }
    
    private function NavCondition()
    {
        $adminNavigacija = false;

        if(!isset($_SESSION['user']))
        {
            $adminNavigacija = false;
        }
        else   
        {
            $userObject = User::UnserializeUser();
            if ($userObject->idPristupa ==1)
            {
                $adminNavigacija = false;
            }
            else 
            {
                $adminNavigacija = true;
            }
        }

        if ($adminNavigacija ===true)
        {
            $this->GetNavigation();
        }
    }
    
      private function GetInner()
    {
       include _PATH_."inner.php";
    }
    
    private function GetPage($page)
    {
        
        global $skolaStrane,$user;       

            
             if (key_exists($page,$skolaStrane))
             include _PGS_.$skolaStrane[$page];
    }

    private function GetEnd()
    {
        include _PATH_."end.php";
    }
    
    private function GetFooter()
    {
        include _PATH_."footer.php";
    }

    public function LoadView($page, $userObject=null)
    {
        global $user,$skolaStrane;
        $access = new AccessLevels($userObject, $page, $skolaStrane);
        $access->PageAccessDenied(); 

        $this->GetHead();
        $this->GetHeader();
        $this->GetKategorije();
//        $this->GetSearch();
        $this->NavCondition();
//        $this->GetInner();
        $this->GetPage($page);
        $this->GetEnd();
        $this->GetFooter();
    }   
}
