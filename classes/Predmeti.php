<?php


class Predmeti extends Database {
    public $fields = "idPredmeta,naziv,slika,cena,opis,idKategorija";
    public $table = "predmeti";
    public $preparedValues = "?,?,?,?,?,?";
    public $primaryKey = "idPredmeta";
    public $filter = "";
    
    public function SetSession($proizvod)
    {  
        Session::SetSession("proizvod", serialize($proizvod));
 
    }
    
    public static function UnserializePredmeti()
    {
        return unserialize($_SESSION['proizvod']);
    }
}
