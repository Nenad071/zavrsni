<?php


class User extends Database
{
    public $fields = "korisnickoIme,ime,prezime,email,sifra";
    public $table = "korisnici";
    public $preparedValues = "?,?,?,?,?";
    public $primaryKey = "idUser";
    public $filter = "";
    public $korpa;
    
    public function login()
    {
        
        $user = $this->GetAll();
        if (count($user)==1)
        {
            $basket = new Basket($user[0]);
           
            $basket = $basket->GetBasketByUser();

            if (is_object($basket))
            {
            
                $korpa = unserialize($basket->content);

                $newKorpa = new Korpa;
                $newKorpa->SetSession($korpa);

                $user[0]->korpa = $korpa;
            }
            
            $this->SetSession($user[0]);
            return true;
        }
        else return false;
    }
    
    public  static function logout()
    { 
        Session :: DestroySession();
    }
    
    public function SetSession($user)
    {  
        Session::SetSession("user", serialize($user));
    }
    
    public static function UnserializeUser()
    {
        
        if (isset($_SESSION['user']))
        {
            return unserialize($_SESSION['user']);
        }
        else return null;
    }
    
    public function AddCart(Korpa $korpa)
    {
        $this->korpa = $korpa;
    }
    
    public function RemoveCart()
    {
        $this->korpa->DestroyCart();
        $this->korpa = null;
    }
}
