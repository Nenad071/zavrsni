<?php

class Session 
{

    public static function Start() 
   {
        session_start();
   
    }
    
    public static function SetSession($key,$value)
    {
        return $_SESSION[$key]=$value;
    }
    public static function GetSession($key,$niz=false)
    {
        if ($_SESSION)
        {
            if ($niz == true)      
            {
                if (isset($_SESSION[$key][$niz]))
                   return $_SESSION[$key][$niz];
            }
            else 
            {
                if (isset($_SESSION[$key]))
                {
                    return $_SESSION[$key];
                }
            }
       
        }
        return null;                
    }
    
    public static function UnsetSession($key)
    {

        unset($_SESSION[$key]);
        
    }
    
    public static function DestroySession()
    {
        session_unset();
        session_destroy();
    }
    
    

}
