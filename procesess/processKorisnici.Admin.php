<?php
//require '../config.php';

$noviClan = null;
$currentUser = null;
$fieldsRules = array
(
    "ime" => array("required" => "", "alpha" => "", "min" => "3"),
    "prezime" => array("required" => "", "alpha" => "", "min" => "3"),
    "korisnickoIme" => array("required" => "", "alphanumeric" => ""),
    "email" => array("required" => "", "email"=>""),
    "sifra" => array("required" => "","number"=>"", "min"=>"4"),
    "idPristupa"=>array("required" => "")
);

$messages = array();

if (isset($_POST['unesi'])) 
{ 
    # moderator ne moze da unosi novog korisnika sa admin ovlascenjima.
    if ($_POST['idPristupa'] == 3 && $user->idPristupa == 2)
    {
        $_POST['idPristupa'] = 2;
    }
    
    $validation = new Validation($_POST,$fieldsRules);    
    
    $res = $validation->validate();
    //var_dump($res);
    if ($res["error"] === true)
    {
        $stringError = "";
        foreach ($res["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    } 
    
    else
    {
    
        $data = $_POST;
        $noviClan = new User();

        $username = inputFilter($data["korisnickoIme"]);
        $email = inputFilter($data["email"]);

        $noviClan->fields = "korisnickoIme,email";
        $noviClan->preparedValues = "?,?";
        $noviClan->filter = "and korisnickoIme= '{$username}' or email = '{$email}'";
        
        $result = $noviClan->GetAll();

        if (count($result) < 1)
        {
            $noviClan->fields = "korisnickoIme,ime,prezime,email,sifra,idPristupa";
            $noviClan->preparedValues = "?,?,?,?,?,?";
            $noviClan->valuesForInsert = $data;

            if ($noviClan->Insert() !== false)
            {
                $messages[]=  "<script>document.write('<span id=\"unos\">Uspesan unos</span>');"
                ."setTimeout(function(){ document.getElementById('unos').innerHTML='';}, 2000);</script>";
            } 
            else 
            {
                $messages[] =  "doslo je do greske pri unosu";
            }
        } 
        else 
        {
            foreach ($result as $resul)
            {
                if ($resul->korisnickoIme == $username)
                {
                    $messages[] = "korisnicko ime je zauzeto. <br>";
                }
                if ($resul->email == $email)
                {
                    $messages[] = "email je zauzet";
                }
            }
        }
    }
}

if (isset($_POST['trazi']))
{
    $currentUser = new User();
    $currentUser->fields = "idUser,korisnickoIme,ime,prezime,sifra,email,idPristupa";
    $currentUser->preparedValues = "?,?,?,?,?,?,?";

    $search = inputFilter($_POST['search']);
    $currentUser->filter = "and korisnickoIme = '{$search}'";
    $currentUser = $currentUser->GetAll();

    if (count($currentUser)<1)
    {
        $messages[]="<span style='color:red'>Trazeni korisnik ne postoji</span>";
    }

}

if (isset($_POST['izmeni']))
{

    $validation = new Validation($_POST,$fieldsRules);
    
    $res = $validation->validate();

    if ($res["error"] === true)
    {
        $stringError = "";
        
        foreach ($res["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    } 
    else 
    {   
        $data = $_POST;
        $username = inputFilter($data["korisnickoIme"]);
        $email = inputFilter($data["email"]);
        $changedUser = new User();
        
        $changedUser->fields = "korisnickoIme,email,idPristupa";
        $changedUser->preparedValues = "?,?,?";
        
        $changedUser->id = $_POST['idUpdate'];
        $changedUser->filter = "and korisnickoIme= '{$username}' or email = '{$email}'";       
        $result = $changedUser->GetAll();
        //$result = $changedUser->GetById();
        
        if (count($result)==1)              
        {  
            if ($user->idPristupa == 2 && $result[0]->idPristupa == 3)
            {
                $messages[]="<span style='color:red'>Moderator nema ovlascenja da menja podatke admin korisnika</span>";  

            }
            
//            elseif ($user->idPristupa == 2 && $result[0]->idPristupa == 3)
//            {
//                $messages[]="<span style='color:red'>Moderator nema ovlascenja da menja podatke admin korisnika</span>";  
//
//            }

            else 
            {
                $changedUser->fields = "korisnickoIme,ime,prezime,sifra,email,idPristupa";
                $changedUser->preparedValues = "?,?,?,?,?,?";
                $changedUser->id=$_POST['idUpdate'];
                $changedUser->valuesForInsert=$data; 
                
                if ($changedUser->Update() !== false)
                {
                    $messages[]= "<script>document.write('<span id=\"izmena\">Uspesna izmena</span>');"
                    . "setTimeout(function(){ document.getElementById('unos').innerHTML='';}, 2000);</script>";
                } 
                else 
                {
                    $messages[] =  "doslo je do greske pri izmeni";
                }
            }   
        }
        else
        {
            $changedUser->fields = "idUser,korisnickoIme,email";
            $changedUser->preparedValues = "?,?,?";
        
            //$changedUser->id = $_POST['idUpdate'];
            $changedUser->filter = "and korisnickoIme= '{$username}' or email = '{$email}'";       
            $result = $changedUser->GetAll();
    
            foreach ($result as $resul)
            {                
                if ($resul->korisnickoIme == $username && $resul->idUser != $_POST['idUpdate'])
                {  
                    $messages[] = "korisnicko ime je zauzeto. <br>";
                }
                if ($resul->email == $email && $resul->idUser != $_POST['idUpdate'])
                {  
                    $messages[] = "email je zauzet";
                }
            }
        }
   }
}

if (isset($_POST['obrisi']))
{
        if (($_POST['idPristupa'] == 3 && $user->idPristupa == 2))
    {
        $messages[] = "Ne mozete obrisati Admina.";
    }
    else
    {
        $deletedUser = new User();
        $deletedUser->id=$_POST['idUpdate'];
        //var_dump($deletedUser);
        $deletedUser->Delete(); 
        
        $messages[] = "Izabrani korisnik je uspesno obrisan";
    }
}
