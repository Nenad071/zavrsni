<?php

require '../config.php';

if (isset($_POST['unesi']))
{
    $validation = new Validation
    (
        $_POST, 
        array
        (
            "ime" => array("required" => "", "alpha" => "","min"=>"3"),
            "prezime"=>array("required" => "", "alpha" => "","min"=>"3"),
            "korisnickoIme" => array("required" => "", "alphanumeric" => "","min"=>"4"),
            "email"=> array("required" => "", "email" => ""),
            "sifra" => array("required" => "", "min"=>"4","max"=>"25"),
            "sifra_confirm" => array ("confirm" => ""),
        )
    );

    $result = $validation ->validate();

    if ($result["error"]===true)
    {
        $stringError = "";
        foreach($result["messages"] as $k=>$v)
        {
            foreach($v as $key=>$value)
            {
                $stringError.= "<span style='color:red'>".$value."</span> <br>";
            }
        }
        header("Location:http://localhost/Zavrsni/index.php?page=3&errorMsg={$stringError}");
    }
    else
    {
        $data = $_POST;
        $user = new User();

        $username = inputFilter($data["korisnickoIme"]);
        $email    = inputFilter($data["email"]);

        $user->fields = "korisnickoIme,email";
        $user->preparedValues = "?,?";
        $user->filter = "and korisnickoIme= '{$username}' or email = '{$email}'";
        $result =  $user->GetAll();

        if (count($result)<1)
        {
            //echo " slobodno za unos";
            $user->fields = "korisnickoIme,ime,prezime,email,sifra";
            $user->valuesForInsert=$data;
            $user->preparedValues = "?,?,?,?,?";

            if ($user->Insert()!==false)
            {
               // header("Location:index.php");
                echo "<script>document.write('Uspesna registracija, uskoro cete biti preusmereni...');"
                . "setTimeout(function(){ window.location='../index.php' }, 2000);</script>";
            }
            else
            {
                echo "doslo je do greske";
            }

        }
        else
        {
           foreach ($result as $resul)
           {
                if ($resul->korisnickoIme ==$username)
                {
                    echo "korisnicko ime je zauzeto. <br>";
                }

                if ($resul->email ==$email)
                {
                    echo "email je zauzet";
                }
           }
        }
    }
}
