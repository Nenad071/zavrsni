<?php

if (!isset($_GET['cat'])) {
    $_GET['cat'] = -1;
}

$categories = new Category();
$categoryData = null;

if (isset($_GET['cat'])) {
    $categories->id = $_GET['cat'];

    if (isset($_GET['obrisi'])) {
        $categories->Delete();
        $predmeti = new Predmeti();
        $predmeti->primaryKey = "idKategorija";

        $predmeti->id = $_GET['cat'];
        $predmeti->Delete();
    }
}

$categories->fields = "naziv,opis";
$categories->preparedValues = "?,?";

if (isset($_POST['unesi'])) {
    $categories->valuesForInsert = $_POST;
    $categories->Insert();
}

if (isset($_POST['izmeni'])) {
    $categories->id = $_GET['cat'];
    $categories->valuesForInsert = $_POST;
    $categories->Update();
}


if (isset($_GET['cat'])) {
    $categoryData = $categories->GetById();

    if (isset($_GET['obrisi'])) {
        $categoryData = null;
    }
}
$categories->fields = "idKategorija,naziv,opis";
$allCat = $categories->GetAll();
