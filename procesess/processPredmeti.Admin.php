<?php

$categories = new Category();
$allCat = $categories->GetAll();
$subjects = new Predmeti();
$subjectsData = null;

$fieldsRules= array
        (
            "naziv" => array("required" => "", "alphanumeric" => "", "min" => "3"),
            "cena" => array("required" => "", "number" => "", "min" => "2"),
            "opis" => array("required" => "", "min" => "50"),
            "idKategorija" => array("required" => "", "minnumber"=>"")
        );

if (isset($_GET['predmet']))
{
    $subjects->id = $_GET['predmet'];
    if (isset($_GET['obrisi']))
    {
        $subjects->Delete();
    }
}

$subjects->fields = "naziv,slika,materijal,cena,opis,idKategorija";
$subjects->preparedValues = "?,?,?,?,?,?";


if (isset($_POST['unesi']))   
{
    $validation = new Validation($_POST, $fieldsRules);
   
    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    } 
    else 
    {
        
        if (isset($_FILES['slika']))
        {
            $subjects->slika = time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);   
        }
        
        if (isset($_FILES['materijal']))
        {
            $subjects->materijal = $_FILES['materijal']['name'];
            move_uploaded_file($_FILES['materijal']['tmp_name'], "uploads/" . $subjects->materijal);        
        }

        $_POST['slika'] = $subjects->slika;
        $_POST['materijal'] = $subjects->materijal;
        $subjects->valuesForInsert = $_POST;
        $subjects->Insert();
    }
}

if (isset($_POST['izmeni']))
{
    $validation = new Validation($_POST,$fieldsRules);

    $result = $validation->validate();

    if ($result["error"] === true)
    {
        $stringError = "";
        foreach ($result["messages"] as $k => $v)
        {
            foreach ($v as $key => $value)
            {
                $stringError .= "<span style='color:red'>" . $value . "</span> <br>";
            }
        }
    } 
    else
    {
        if ($_FILES['slika']['tmp_name'] !== "" && $_FILES['materijal']['name'] !== "")
        {   
            $subjects->slika = time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);
            $_POST['slika'] = $subjects->slika;
            $subjects->materijal = $_FILES['materijal']['name'];
            move_uploaded_file($_FILES['materijal']['name'] == "", "uploads/" . $subjects->materijal);
            $_POST['slika'] = $subjects->materijal;
        }

        elseif ($_FILES['slika']['tmp_name'] !== "" && $_FILES['materijal']['name'] == "")
        {
            $subjects->fields = "naziv,cena,slika, opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?,?";
            $subjects->slika = time() . ".png";
            move_uploaded_file($_FILES['slika']['tmp_name'], "files/" . $subjects->slika);
            $_POST['slika'] = $subjects->slika;
        }

        elseif ($_FILES['materijal']['name'] !== "" && $_FILES['slika']['tmp_name'] == "" )
        {
            $subjects->fields = "naziv,cena,materijal,opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?,?";
            $subjects->materijal = $_FILES['materijal']['name'];
            move_uploaded_file($_FILES['materijal']['name'] == "", "uploads/" . $subjects->materijal);
            $_POST['slika'] = $subjects->materijal;
        }  
        else
        {
            $subjects->fields = "naziv,cena,opis,idKategorija";
            $subjects->preparedValues = "?,?,?,?"; 

        }

        $subjects->id = $_GET['predmet'];
        $subjects->valuesForInsert = $_POST;
        $subjects->Update();        
    }
}

if (isset($_GET['predmet']))
{
    $subjectsData = $subjects->GetById();
    if (isset($_GET['obrisi']))
    {
        $subjectsData = null;
    }
}

$subjects->fields = "idPredmeta,naziv,slika,cena,materijal,opis,idKategorija";
$subjects->preparedValues = "?,?,?,?,?,?,?";

$allSubjects = $subjects->GetAll();