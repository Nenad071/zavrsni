<?php

require "../config.php";

if (isset($_SESSION['user']))
{
        $user = User::UnserializeUser();
        $basket = new Basket($user);

        if (!is_object($basket->GetBasketByUser()) && $basket->GetBasketByUser()===false)
        {
            if ($user->korpa!==null && $user->korpa!==false)    
            {
                 $basket->InsertIntoBasket();
            }
        }
        else
        {
            if (empty($user->korpa->nizProizvoda))
            {
                $basket->Delete();
            }
            else
            {
                $basketNiz['content']= serialize($user->korpa);
                $basket->fields = "content";
                $basket->preparedValues = "?";
                $basket->valuesForInsert = $basketNiz;
                $basket->Update();
            }
        }
         
    $user = User::logout();
    header ("Location: ../index.php");
}     

//header ("Location: ../../index.php");

//$user = User::logout();
//var_dump($_SESSION);