<?php

include 'procesess/processKategorije.Admin.php';

?>

<div class="9u mobileUI-main-content">
    <div id="content">
<!--        <div class="centerForm">-->
            <b>Admin - Kategorije</b><br><br>
            
            <form class="form-style-9" action="" method="post">
                Izaberite Kategoriju:<br><br>
                <ul>
                    <li>
                    <select class="field-style field-full align-none" onchange="window.location = '?page=11&cat=' + this.value" name="izbor_kategorije">
                        <option value="-1">Izaberite Kategoriju</option>
<?php
foreach ($allCat as $cat) {
    $selected = "";
    if (isset($_GET['cat'])) {
        $selected = ($_GET['cat'] == $cat->idKategorija) ? " selected" : "";
    }
    echo "<option" . $selected . " value='{$cat->idKategorija}'> {$cat->naziv}</option><br> ";
}
?>
                    </select></li>
<?php
    $cData = $categoryData === null || (empty($categoryData));
?>
                        
                        
                    <li>
                        <input type="text" name="naziv" value="<?= $cData ? "" : $categoryData->naziv ?>" class="field-style field-full align-none" placeholder="Naziv">
                    </li>
                    
                                            
                    <li>
                        <input type="text" name="opis" value="<?= $cData ? "" : $categoryData->opis ?>" class="field-style field-full align-none" placeholder="Opis">
                    </li>
        
                    <li>
                    <input type="submit" name="unesi" value="UNESI">
                    <input type="submit" name="izmeni" value="IZMENI">  
                    <input type="button" name="obrisi"  value="OBRISI" onclick="Obrisi()">
                    </li>
                </ul>
            </form>
        </div>
    </div> 


<script>
    function Obrisi()
    {
        var potvrda = confirm("Da li ste sigurni da zelite da obrisete ovaj proizvod");

        if (potvrda)
        {
            window.location = "?page=11&cat=<?= $_GET['cat'] ?>&obrisi=1";
        }
    }
</script>